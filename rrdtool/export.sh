#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DB=$DIR/db/temperatures.rrd
EXPORT=$DIR/export

rrdtool xport --json -s now-3h -e now --step 300 \
DEF:a=$DB:temps1:AVERAGE \
DEF:b=$DB:temps2:AVERAGE \
DEF:c=$DB:temps3:AVERAGE \
DEF:d=$DB:temps4:AVERAGE \
DEF:e=$DB:temps5:AVERAGE \
DEF:f=$DB:temps6:AVERAGE \
XPORT:a:"Inside" \
XPORT:b:"Outside" \
XPORT:c:"Heating output" \
XPORT:d:"DHW return" \
XPORT:e:"DHW output" \
XPORT:f:"Heating return" > $EXPORT/temperature3h.json

rrdtool xport --json -s now-24h -e now --step 900 \
DEF:a=$DB:temps1:AVERAGE \
DEF:b=$DB:temps2:AVERAGE \
DEF:c=$DB:temps3:AVERAGE \
DEF:d=$DB:temps4:AVERAGE \
DEF:e=$DB:temps5:AVERAGE \
DEF:f=$DB:temps6:AVERAGE \
XPORT:a:"Inside" \
XPORT:b:"Outside" \
XPORT:c:"Heating output" \
XPORT:d:"DHW return" \
XPORT:e:"DHW output" \
XPORT:f:"Heating return" > $EXPORT/temperature24h.json

rrdtool xport --json -s now-48h -e now --step 1800 \
DEF:a=$DB:temps1:AVERAGE \
DEF:b=$DB:temps2:AVERAGE \
DEF:c=$DB:temps3:AVERAGE \
DEF:d=$DB:temps4:AVERAGE \
DEF:e=$DB:temps5:AVERAGE \
DEF:f=$DB:temps6:AVERAGE \
XPORT:a:"Inside" \
XPORT:b:"Outside" \
XPORT:c:"Heating output" \
XPORT:d:"DHW return" \
XPORT:e:"DHW output" \
XPORT:f:"Heating return" > $EXPORT/temperature48h.json

rrdtool xport --json -s now-8d -e now --step 7200 \
DEF:a=$DB:temps1:AVERAGE \
DEF:b=$DB:temps2:AVERAGE \
DEF:c=$DB:temps3:AVERAGE \
DEF:d=$DB:temps4:AVERAGE \
DEF:e=$DB:temps5:AVERAGE \
DEF:f=$DB:temps6:AVERAGE \
XPORT:a:"Inside" \
XPORT:b:"Outside" \
XPORT:c:"Heating output" \
XPORT:d:"DHW return" \
XPORT:e:"DHW output" \
XPORT:f:"Heating return" > $EXPORT/temperature1w.json

rrdtool xport --json -s now-1month -e now --step 10800 \
DEF:a=$DB:temps1:AVERAGE \
DEF:b=$DB:temps2:AVERAGE \
DEF:c=$DB:temps3:AVERAGE \
DEF:d=$DB:temps4:AVERAGE \
DEF:e=$DB:temps5:AVERAGE \
DEF:f=$DB:temps6:AVERAGE \
XPORT:a:"Inside" \
XPORT:b:"Outside" \
XPORT:c:"Heating output" \
XPORT:d:"DHW return" \
XPORT:e:"DHW output" \
XPORT:f:"Heating return" > $EXPORT/temperature1m.json

rrdtool xport --json -s now-3month -e now --step 43200 \
DEF:a=$DB:temps1:AVERAGE \
DEF:b=$DB:temps2:AVERAGE \
DEF:c=$DB:temps3:AVERAGE \
DEF:d=$DB:temps4:AVERAGE \
DEF:e=$DB:temps5:AVERAGE \
DEF:f=$DB:temps6:AVERAGE \
XPORT:a:"Inside" \
XPORT:b:"Outside" \
XPORT:c:"Heating output" \
XPORT:d:"DHW return" \
XPORT:e:"DHW output" \
XPORT:f:"Heating return" > $EXPORT/temperature3m.json

rrdtool xport --json -s now-1y -e now --step 86400 \
DEF:a=$DB:temps1:AVERAGE \
DEF:b=$DB:temps2:AVERAGE \
DEF:c=$DB:temps3:AVERAGE \
DEF:d=$DB:temps4:AVERAGE \
DEF:e=$DB:temps5:AVERAGE \
DEF:f=$DB:temps6:AVERAGE \
XPORT:a:"Inside" \
XPORT:b:"Outside" \
XPORT:c:"Heating output" \
XPORT:d:"DHW return" \
XPORT:e:"DHW output" \
XPORT:f:"Heating return" > $EXPORT/temperature1y.json
