#!/usr/bin/env python3

import os
import time
import json
import flotilla
from requests import get
#from homeassistant_api import Client

PM25_SENSOR = "sensor.ikea_vindriktning_pm2_5_sensor"
URL = "http://localhost:8123/api/states/" + PM25_SENSOR
TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIwNjk5NWYyNmM5NGY0YTk1YTRlN2U2MTY4YjIwNWVlOSIsImlhdCI6MTY1MjY4ODIwNywiZXhwIjoxOTY4MDQ4MjA3fQ.IIE45VFrDQbtXvg7bE3XbJPoPdwY_CSFdWskxO_yJEE"
HEADERS = {
    "Authorization": "Bearer " + TOKEN,
    "content-type": "application/json",
}

print("""
This example will display temperature and current time on the Number display
""")

dock = flotilla.Client()
#client = Client(url, token)

light_sensor = dock.first(flotilla.Light)
weather_sensor = dock.first(flotilla.Weather)
number_display_1 = dock.nth(flotilla.Number, 1)
number_display_2 = dock.nth(flotilla.Number, 2)
number_display_3 = dock.nth(flotilla.Number, 3)
number_display_4 = dock.nth(flotilla.Number, 4)


def init():
    initial_brightness = 10
    number_display_1.set_brightness(initial_brightness)
    number_display_2.set_brightness(initial_brightness)
    number_display_3.set_brightness(initial_brightness)
    number_display_4.set_brightness(initial_brightness)


def get_pm25():
    try:
        response = get(URL, headers=HEADERS)
        if response.ok:
            jdata = json.loads(response.text)
            if "state" in jdata:
                return jdata["state"].ljust(4, " ")
    except:
        pass
    return "--  "


def loop():
    while True:
        brightness = int(light_sensor.light / 2)

        if brightness > 255:
            brightness = 255
        if brightness < 1:
            brightness = 1

        for module in dock.available.values():
            if module.is_a(flotilla.Number):
                module.set_brightness(brightness)

        #client = Client(url, token)
        #entity_groups = client.get_entities()
        #pm25 = client.get_entity(entity_id=pm25_sensor)
        #states = client.get_states()

        number_display_1.set_string(get_pm25())
        number_display_1.update()

        number_display_2.set_temp(weather_sensor.temperature)
        number_display_2.update()

        number_display_3.set_current_time()
        number_display_3.update()

        pressure = weather_sensor.pressure

        if pressure > 9999:
            pressure = pressure / 10

        number_display_4.set_number(int(pressure))
        number_display_4.update()

        time.sleep(5)


def main():
    """Main is main."""

    while not dock.ready:
        pass

    init()

    try:
        loop()
    except KeyboardInterrupt:
        dock.stop()


if __name__ == "__main__":
    main()
