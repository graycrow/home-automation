﻿function onPulseOn(e) {
    print(e.time - lastTime);
    lastTime = e.time;
}

function onPulseOff(e) {
    lastTime = e.time;
}

function blinker() {
    var led = NodeMCU.D4;
    setInterval(function() {
        digitalWrite(led, 0);
        setTimeout(function() {
            digitalWrite(led, 1);
        }, 100);
    }, 2000);
}

//E.on("init", function () {blinker();});

pinMode(D0, "input_pullup");
digitalWrite(D2, 0);
pinMode(D4, "input_pullup");
pinMode(D5, "input_pullup");
pinMode(D12, "input_pullup");
pinMode(D13, "input_pullup");
pinMode(D14, "input_pullup");
pinMode(D15, "input_pullup");
blinker();
setWatch(onPulseOff, A0, { repeat: true, edge: "rising" });
setWatch(onPulseOn, A0, { repeat: true, edge: "falling" });
