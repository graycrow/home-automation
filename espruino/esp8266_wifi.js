﻿var wifi = require("Wifi");
wifi.connect(
    "graycrow",
    { password: "streamlatus" },
    function(err) {
        if (err) {
            console.log("Not connected. err=", err);
        } else {
            var hostname = "espruino";
            console.log("Connected");
            wifi.stopAP();
            wifi.setHostname(hostname);
            wifi.save();
            console.log(wifi.getStatus());
            console.log(wifi.getIP());
        }
    }
);
