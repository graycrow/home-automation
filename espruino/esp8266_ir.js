﻿var led = Pin(NodeMCU.D4);
var pin = Pin(NodeMCU.D5);

var connect = function(pin, callback, options) {
    var pulseGap = true;
    if (options instanceof Object) {
        if (options.usePulseLength) pulseGap = false;
    }
    // pullup on the pin - most IR receivers don't have this
    pinMode(pin, "input_pullup");
    // the actual code
    var code = "";
    // the timeout that will trigger the callback after the last bit
    var timeout;
    // set our callback to happen whenever pin goes low (eg, whenever pulse starts)
    setWatch(
        function(e) {
            var d = e.time - e.lastTime;
            if (timeout) {
                clearTimeout(timeout);
                timeout = undefined;
            }
            if (d > 0.04 || code.length > 100) {
                // a gap between transmissions, or code getting long
                if (code !== "") callback(code);
                code = "";
            } else {
                code += 0 | (d > 0.0008);
                // queue a timeout so after we stop getting bits, we execute the callback
                timeout = setTimeout(function() {
                    timeout = undefined;
                    if (code !== "") callback(code);
                    code = "";
                }, 50);
            }
        },
        pin,
        { repeat: true, edge: pulseGap ? "falling" : "rising" }
    );
};

// require("IRReceiver").connect(D5, function(code) {
connect(pin, function(code) {
    print(code);
    digitalPulse(led, 0, 20);
});
