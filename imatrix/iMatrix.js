#!/usr/bin/env node
/* global process, __dirname */
/* jslint node: true */

"use strict";

process.binding("http_parser").HTTPParser = require("http-parser-js").HTTPParser;

const ThingSpeakClient = require("thingspeakclient");
const RRDTool = require("./rrd.js");
const spreadsheet = require("edit-google-spreadsheet");
const http = require("http");
const moment = require("moment");
const path = require("path");
const config = {
    dateTimeFormat: "YYYY/MM/DD HH:mm:ss",
    spreadsheetName: "iMatrix-4",
    //spreadsheetId: "1rAUZxs3QhOFb2_-faxeVTNBGCTVQG_qcjgjwBYDf9N4", //iMatrix-1
    //spreadsheetId: "1RXdEZXnaRgff9g958b0DVsL_-q2f-IGwgBbglGIlCcE", //iMatrix-2
    //spreadsheetId: "16iCCns4iv19KbvwDVCh-t1dzqJvHIe87-cTVIPvjPn8", //iMatrix-3
    spreadsheetId: "10V2sdAJnLlhqBYyA0drDSk2u_8znEJCF1HW8xanVjFU", //iMatrix-4
    worksheetName: "Sheet1",
    worksheetId: "od6",
    email: "450454007784-m5tad2q2l4hejg2ml29jb40nm3q3h89n@developer.gserviceaccount.com",
    keyRelativePath: "../keys",
    keyFile: "iMatrix-2f3b8c855fb0.pem",
    thinkSpeakUrl: "https://api.thingspeak.com",
    thingSpeakChannelId: 38522,
    thinkSpeakWriteApiKey: "8QFS7NZXDWH6731N",
    rrdDatabaseRelativePath: "../rrdtool/db",
    rrdDatabaseFile: "temperatures.rrd",
    ip: "127.0.0.1",
    port: 8083,
    sensors: [[11, 2], [12, 2], [10, 3], [10, 4], [10, 5], [10, 6]] // device, instance
};

const getTemp = (device, instance) => {
    return new Promise((resolve, reject) => {
        const options = {
            host: config.ip,
            port: config.port,
            path: `/ZWaveAPI/Run/devices[${device}].instances[${instance}].commandClasses[49].data[1].val.value`
        };

        const req = http.request(options, (res) => {
            let body = "";
            res.on("data", (d) => {
                body += d;
            });
            res.on("end", () => {
                const data = parseFloat(body);
                if (res.statusCode === 200 && !isNaN(data)) {
                    resolve(data.toFixed(2));
                }
                resolve(null);
            });
        });

        req.on("error", (e) => {
            //reject(e.message);
            resolve(null);
        });

        req.end();
    });
};

const getTimeStamp = () => moment().format(config.dateTimeFormat);

const getData = () => {
    return new Promise((resolve, reject) => {
        const promises = config.sensors.map((element) => getTemp(element[0], element[1]));

        Promise.all(promises).then((temp) => {
            const data = { 1: getTimeStamp() };
            for (let i = 0; i < temp.length; i++) {
                data[i + 2] = temp[i];
            }
            resolve(data);
        }, (err) => {
            reject(err);
        });
    });
};

const uploadSpreadsheetData = (rowData) => {
    if (rowData) {
        spreadsheet.load({
            debug: true,
            //spreadsheetName: config.spreadsheetName,
            //worksheetName: config.worksheetName,
            spreadsheetId: config.spreadsheetId,
            worksheetId: config.worksheetId,
            oauth: {
                email: config.email,
                keyFile: path.resolve(__dirname, config.keyRelativePath, config.keyFile)
            }
        }, function sheetReady(err, spreadsheet) {
            if (err) {
                throw err;
            }

            spreadsheet.metadata((err, metadata) => {
                if (err) {
                    throw err;
                }

                const data = {};
                const nextRow = metadata.rowCount + 1;
                data[nextRow] = rowData;
                spreadsheet.add(data);
                spreadsheet.send({ autoSize: true }, (err) => {
                    if (err) {
                        throw err;
                    }
                    console.log(`Updated row ${nextRow}, at ${data[nextRow]["1"]}`);
                });
            });
        });
    } else {
        console.log("Nothing to upload");
    }
};

const uploadThingspeakData = (data) => {
    if (data) {
        const channelData = {};

        for (let i = 2; i <= Object.keys(data).length; ++i) {
            channelData[`field${(i - 1) }`] = data[i];
        }

        const client = new ThingSpeakClient({ useTimeoutMode: false });
        client.attachChannel(config.thingSpeakChannelId, { writeKey: config.thinkSpeakWriteApiKey }, () => {
            client.updateChannel(config.thingSpeakChannelId, channelData, (err, resp) => {
                if (!err && resp > 0) {
                    console.log(`Updated thingspeak at ${data["1"]}`);
                }
            });
        });
    } else {
        console.log("Nothing to upload");
    }
};

const updateRrdtoolDB = (data) => {
    if (data) {
        const keys = Object.keys(data);
        const rrdData = keys
            .map((key) => key === "1" ? "N" : data[key] || "U")
            .concat(Array.apply(null, new Array(9 - keys.length)).map(String.prototype.valueOf, "U"))
            .join(":");
        const dbPath = path.resolve(__dirname, config.rrdDatabaseRelativePath, config.rrdDatabaseFile);
        const rrd = new RRDTool();
        rrd.update(dbPath, rrdData, (err, msg) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`Updated ${config.rrdDatabaseFile} at ${data["1"]}`);
            }
        });
    } else {
        console.log("Nothing to upload");
    }
};

const main = function () {
    getData().then((data) => {
        if (data) {
            //console.log(data);
            updateRrdtoolDB(data);
            uploadThingspeakData(data);
            uploadSpreadsheetData(data);
        }
    });
};

main();
