﻿/* jslint node: true */

"use strict";

/*
 The aim of this module is to provide an API which matches as closely as possible the intent of the rrdtool CLI
 interface.
 */
const exec = require("child_process").exec;

class RRDTool {
    /**
     * @param options
     * @constructor
     */
    constructor(options) {
        this.options = options || {};
        if (this.options.hasOwnProperty("path")) {
            this.path = this.options.path;
        }
    }
    
    /**
     * Method to exec the rrdtool command with the supplied args.
     *
     * @param argsArray
     * @param onErr
     * @param onSuccess
     * @private
     */
    _rrdExec(argsArray, onErr, onSuccess) {
        const path = this.getPath(),
            args = [path].concat(argsArray).join(" ");

        // TODO: keep a track of child processes.
        const child = exec(args, (err, stdout, stderr) => {
            // rrdtool currently doesn't seem to use stderr at all..
            if (err !== null) {
                onErr(err);
            } else {
                onSuccess(stdout);
            }
        });
    }
    
    /**
     * Return the configured path or the default one.
     *
     * @return {*|string}
     */
    getPath() {
        return this.path || "rrdtool";
    }

    /**
     * Standard javascript date to unix time conversion function. As with moment.js this value is floored to the nearest
     * second, and does not include a milliseconds component.
     *
     * @param date
     * @return {Number}
     */
    unixTime(date) {
        return Math.floor(date / 1000);
    }
    
    /**
     * Just call the rrdtool command to ensure it is installed.
     *
     * @param callback
     */
    version(callback) {
        // supplying no args results in usage being printed to stdout
        this._rrdExec(["--help"], callback, (data) => {
            const result = data.match(/^RRDtool ([0-9\.]+)/); //either null or contains items.
            if (!result) {
                callback(new Error("Unable to locate version string."));
            } else {
                callback(null, result[0]);
            }
        });
    }

    /**
     * Uses the info action within rrdtool to provide a summary of the supplied rrd file content.
     *
     * @param filePath
     * @param callback
     */
    info(filePath, callback) {
        const args = ["info", filePath];

        this._rrdExec(args, callback, (data) => {
            const info = {};
            const lines = data.split("\n");

            lines.forEach((line) => {
                const result = line.match(/^ds\[([a-zA-Z0-9_]+)\]\.([a-zA-Z0-9_]+) = (.*)$/);
                if (result && result.length == 4) {
                    if (!info[result[1]]) {
                        info[result[1]] = {};
                    }
                    const value = Number(result[3]);
                    if (Number.isNaN(value)) {
                        info[result[1]][result[2]] = result[3].replace(/['"]/g, "");
                    } else {
                        info[result[1]][result[2]] = Number(result[3]);
                    }
                }
            });
            callback(null, info);
        });
    }

    /**
     * Fetch will analyze the RRD and try to retrieve the data in the resolution requested.
     *
     * @param filePath
     * @param cf - Consolidation function, possible values are  AVERAGE, MIN, MAX or LAST
     * @param startTimestamp - The start date as a unix timestamp
     * @param endTimestamp - The end date as a unix timestamp
     * @param resolution - The interval you want the values to have (seconds per value).
     * @param callback
     */
    fetch(filePath, cf, startTimestamp, endTimestamp, resolution, callback) {
        const args = ["fetch", filePath, cf, "--start", startTimestamp, "--end", endTimestamp];

        if (resolution) {
            args.push("--resolution", resolution);
        }

        this._rrdExec(args, callback, (data) => {
            const lines = data.split("\n"),
                headerLine = lines.shift(),
                emptyLineFilter = (val) => (val !== ""),
                convertToNumber = (val) => (Number(val)),
                splitLineOfNumbers = (line) => line.split(/[:\ ]+/).map(convertToNumber),
                results = {
                    headers: headerLine.split(/\ +/).filter(emptyLineFilter),
                    data: lines.filter(emptyLineFilter).map(splitLineOfNumbers)
                };

            // prepend timestamp column
            results.headers.unshift("timestamp");
            callback(null, results);
        });
    }
    
    /**
     * Create a new RRD file.
     *
     * @param filePath
     * @param ds - An array of data sources, this will typically consist of one or more metrics.
     * @param rra = An array of round robin archives.
     * @param callback
     */
    create(filePath, ds, rra, callback) {
        const args = ["create", filePath].concat(ds).concat(rra);

        this._rrdExec(args, callback, (data) => {
            callback(null, data);
        });
    }

    /**
     * Update the RRD file adding one or more values.
     *
     * @param filePath
     * @param values - An array of values to update.
     * @param callback
     */
    update(filePath, values, callback) {
        const args = ["update", filePath].concat(values);

        this._rrdExec(args, callback, (data) => {
            callback(null, data);
        });
    }
}

module.exports = RRDTool;
