#!/usr/bin/perl
use LWP::UserAgent;
 
my $metar_url = 'http://weather.noaa.gov/pub/data/observations/metar/stations/LKPR.TXT';
#my $metar_url = 'http://weather.noaa.gov/pub/data/observations/metar/stations/LKKB.TXT';
 
my $ua = new LWP::UserAgent;
$ua->timeout(120);
my $request = new HTTP::Request('GET', $metar_url);
my $response = $ua->request($request);
my $metar= $response->content();

$metar =~ /([\s|M])(\d{2})\//g;
$outtemp = ($1 eq 'M') ? $2 * -1 : $2; #'M' in a METAR report signifies below 0 temps
$outtemp = $outtemp + 0;
print "Outside temperature: $outtemp degrees centigree.";