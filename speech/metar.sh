#!/bin/bash

export DISPLAY=:0
export LANG=ru_RU.UTF-8

#sayit () { espeak -ven -s130; }
#sayit () { espeak -vru -s130; }
#sayit () { festival --tts; }
#sayit () { festival --tts --language russian; }
#sayit () { festival_client --ttw | aplay -q; }
sayit () { RHVoice | aplay -q; }
#sayit () { RHVoice -W Elena | aplay -q; }

check_date ()
{
WEEKDAY=`date +%A`
MONTH=`date +%m`
DAY=`date +%d`
HOUR=`date +%H`
MIN=`date +%M`

case $DAY in
    01) LC_DAY="первое" ;;
    02) LC_DAY="второе" ;;
    03) LC_DAY="третье" ;;
    04) LC_DAY="четвертое" ;;
    05) LC_DAY="пятое" ;;
    06) LC_DAY="шестое" ;;
    07) LC_DAY="седьмое" ;;
    08) LC_DAY="восьмое" ;;
    09) LC_DAY="девятое" ;;
    10) LC_DAY="десятое" ;;
    11) LC_DAY="одинадцатое" ;;
    12) LC_DAY="двенадцатое" ;;
    13) LC_DAY="тринадцатое" ;;
    14) LC_DAY="четырнадцатое" ;;
    15) LC_DAY="пятнадцатое" ;;
    16) LC_DAY="шестнадцатое" ;;
    17) LC_DAY="семнадцатое" ;;
    18) LC_DAY="восемнадцатое" ;;
    19) LC_DAY="девятнадцатое" ;;
    20) LC_DAY="двадцатое" ;;
    21) LC_DAY="двадцать первое" ;;
    22) LC_DAY="двадцать второе" ;;
    23) LC_DAY="двадцать третье" ;;
    24) LC_DAY="двадцать четвертое" ;;
    25) LC_DAY="двадцать пятое" ;;
    26) LC_DAY="двадцать шестое" ;;
    27) LC_DAY="двадцать седьмое" ;;
    28) LC_DAY="двадцать восьмое" ;;
    29) LC_DAY="двадцать девятое" ;;
    30) LC_DAY="тридцатое" ;;
    31) LC_DAY="тридцать первое" ;;
esac

if [[ "$MONTH" = 01 ]]; then LC_MONTH="января"
elif [[ "$MONTH" = 02 ]]; then LC_MONTH="февраля"
elif [[ "$MONTH" = 03 ]]; then LC_MONTH="марта"
elif [[ "$MONTH" = 04 ]]; then LC_MONTH="апреля"
elif [[ "$MONTH" = 05 ]]; then LC_MONTH="мая"
elif [[ "$MONTH" = 06 ]]; then LC_MONTH="июня"
elif [[ "$MONTH" = 07 ]]; then LC_MONTH="июля"
elif [[ "$MONTH" = 08 ]]; then LC_MONTH="августа"
elif [[ "$MONTH" = 09 ]]; then LC_MONTH="сентября"
elif [[ "$MONTH" = 10 ]]; then LC_MONTH="октября"
elif [[ "$MONTH" = 11 ]]; then LC_MONTH="ноября"
elif [[ "$MONTH" = 12 ]]; then LC_MONTH="декабря"
fi

DATE="$WEEKDAY, $LC_DAY $LC_MONTH"

if [[ "$HOUR" = 1[1234] || "$HOUR" = ?[056789] ]]; then LC_HOUR="часов"
elif [[ "$HOUR" = ?[234] ]]; then LC_HOUR="час+а"
elif [[ "$HOUR" = ?1 ]]; then LC_HOUR="час"
else LC_HOUR="значение не определено"
fi

if [[ "$MIN" = 1[1234] || "$MIN" = ?[056789] ]]; then LC_MINUTE="минут"
elif [[ "$MIN" = ?[34] ]]; then LC_MINUTE="минуты"
else LC_MINUTE="значение не определено"
fi

if [[ "$MIN" = 01 ]]; then TIME="$HOUR $LC_HOUR однa минута"
elif [[ "$MIN" = 11 ]]; then TIME="$HOUR $LC_HOUR $MIN $LC_MINUTE"
elif [[ "$MIN" = ?1 ]]; then TIME="$HOUR $LC_HOUR $(($MIN-1)) одна минута"
elif [[ "$MIN" = 02 ]]; then TIME="$HOUR $LC_HOUR две минуты"
elif [[ "$MIN" = 12 ]]; then TIME="$HOUR $LC_HOUR $MIN $LC_MINUTE"
elif [[ "$MIN" = ?2 ]]; then TIME="$HOUR $LC_HOUR $(($MIN-2)) две минуты"
elif [[ "$MIN" = 00 ]]; then TIME="$HOUR $LC_HOUR ровно"
else TIME="$HOUR $LC_HOUR $MIN $LC_MINUTE"
fi

if [[ "$HOUR" = 08 ]] || [[ "$HOUR" -ge 06 && "$HOUR" -le 11 ]]; then
HELLO="Доброе утро"
elif [[ "$HOUR" -ge 12 && "$HOUR" -le 17 ]]; then
HELLO="Добрый день"
elif [[ "$HOUR" -ge 18 && "$HOUR" -le 23 ]]; then
HELLO="Добрый вечер"
elif [[ "$HOUR" -ge 00 && "$HOUR" -le 05 ]]; then
HELLO="Доброй ночи"
else HELLO="Привет"
fi

}

IN_TEMP="`wget -qO- http://127.0.0.1:8083/ZWaveAPI/Run/devices[11].instances[2].commandClasses[49].data[1].val.value`"
#IN_TEMP="20

IN_DEGREE="`echo $IN_TEMP | grep -E -o '[-+]?([0-9]*\.[0-9]+|[0-9]+)'`"
IN_DEGREE="`echo \"($IN_DEGREE + 0.5)/1\" | bc`"

if [[ "$IN_DEGREE" = 1[1234] || "$IN_DEGREE" = *[056789] ]]; then LC_IN_DEGREE="градусов"
elif [[ "$IN_DEGREE" = *[234] ]]; then LC_IN_DEGREE="градуса"
elif [[ "$IN_DEGREE" = *1 ]]; then LC_IN_DEGREE="градус"
else LC_IN_DEGREE="не определена"
fi

METAR="`/home/pi/speech/metar.js/metar.js`"

check_date
#echo "$HELLO. Сегодня $DATE. Время $TIME. Температура за окном $DEGREE $TEMP_SIGN $LC_DEGREE. Температура в комнате $IN_DEGREE $LC_IN_DEGREE. $METAR" | sayit
echo "$HELLO. Сегодня $DATE. Время $TIME. Температура в комнате $IN_DEGREE $LC_IN_DEGREE. $METAR" | sayit
#echo "$METAR. Температура в комнате $IN_DEGREE $LC_IN_DEGREE." | sayit
