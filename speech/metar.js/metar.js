#!/usr/bin/env node
var fs = require("fs"),
    http = require("http"),
    moment = require("moment"),
    path = require("path"),
    spawn = require("child_process").spawn,
    App = App || {};

App.settings =
{
    commands: ["echo",
            "espeak -ven -s130",
            "espeak -vru -s130",
            "festival --tts",
            "festival --tts --language russian",
            "festival_client --ttw | aplay -q",
            "RHVoice | aplay -q",
            "RHVoice -W Elena | aplay -q"],
    url: "http://weather.noaa.gov/pub/data/observations/metar/stations/LKKB.TXT"
};

App.utils = (function ()
{
    var degreeEnd = ["градус", "градуса", "градусов"],
        meterEnd = ["метр", "метра", "метров"],
        knotEnd = ["узел", "узла", "узлов"],
        feetEnd = ["фут", "фута", "футов"],
        mileEnd = ["миля", "мили", "миль"],
        minuteEnd = ["минута", "минуты", "минут"],
        minute2End = ["минуту", "минуты", "минут"],
        hourEnd = ["час", "часа", "часов"],
        pascalEnd = ["паскаль", "паскаля", "паскалей"],
        inchEnd = ["дюйм", "дюйма", "дюймов"],
        intEnd = ["целая", "целые", "целых"],
        tenthEnd = ["десятая", "десятые", "десятых"],
        hundrEnd = ["сотая", "сотых", "сотых"],
        compassDirs,
        pressureConversion,
        speedConversion,
        temperatureConversion,
        self = this;

    compassDirs = {
        "северный":  0.0,
        "северный-северо-восточный": 22.5,
        "северо-восточный": 45.0,
        "восточный-северо-восточный": 67.5,
        "восточный": 90.0,
        "восточный-юго-восточный": 112.5,
        "юго-восточный": 135.0,
        "южный-юго-восточный": 157.5,
        "южный": 180.0,
        "южный-юго-западный": 202.5,
        "юго-западный": 225.0,
        "западный-юго-западный": 247.5,
        "западный": 270.0,
        "западный-северо-западный":292.5,
        "северо-западный": 315.0,
        "северный-северо-западный": 337.5
    };

    pressureConversion = {
        units: ["hpa", "mmhg", "inhg"],
        table: [[1, 0.750061683, 0.0295299830714], [1.33322368, 1, 0.03937023], [33.863886666718315, 25.399999705, 1]],
        ending: {
            hpa: ["hectopascals", ""],
            mmhg: ["milimeters", " ртутного столба"],
            inhg: ["inches", " ртутного столба"]
        }
    };

    speedConversion = {
        units: ["kt", "kmh", "mps"],
        table: [[1, 1.852, 0.514444], [0.539957, 1, 0.277778], [1.94384, 3.6, 1]],
        ending: {
            kt: ["knots", ""],
            kmh: ["kilometers", " в час"],
            mps: ["meters", " в секунду"]
        }
    };

    temperatureConversion = {
        units: ["c", "f"],
        table: [[1, 1.8], [1 / 1.8, 1]],
        shift: [[0, 32], [-32, 0]],
        ending: {
            c: ["degrees", " цельсия"],
            f: ["degrees", " фаренгейта"]
        }
    };

    var isInt = function (num)
    {
        return num % 1 === 0;
    };

    var isNumDigit = function (ch)
    {
        return ((ch == '0') || (ch == '1') || (ch == '2') || (ch == '3') ||
             (ch == '4') || (ch == '5') || (ch == '6') || (ch == '7') ||
             (ch == '8') || (ch == '9'));
    };

    var isAlphabeticChar = function (ch)
    {
        return ((ch >= 'A') && (ch <= 'Z'));
    };

    var getNumEnding = function (iNumber, aEndings)
    {
        var sEnding, i;
        iNumber = Math.abs(iNumber % 100);
        if (iNumber >= 11 && iNumber <= 19)
        {
            sEnding = aEndings[2];
        }
        else
        {
            i = iNumber % 10;
            switch (i)
            {
                case (1): sEnding = aEndings[0]; break;
                case (2):
                case (3):
                case (4): sEnding = aEndings[1]; break;
                default: sEnding = aEndings[2];
            }
        }
        return sEnding;
    };

    // если числительное заканчивается на один или два - сказать две целых вместо два целых
    var fixOneTwoFloat = function (num)
    {
        var last = parseInt(num[num.length - 1], 10),
            base = Math.abs(num) - last;

        if (num.length > 1 & parseInt(num[num.length - 2], 10) === 1)
        {
            return num;
        }

        switch (last)
        {
            case 0:
                return Math.abs(num);
            case 1:
                return (base === 0 ? num < 0 ? "минус " : "" : base + " ") + "одна";
            case 2:
                return (base === 0 ? num < 0 ? "минус " : "" : base + " ") + "две";
            default:
                return num;
        }
    };

    var sayFloatNum = function (num)
    {
        var parts;
        if (!isNaN(num) && typeof num == "number")
        {
            if (isInt(num))
            {
                return num === 0 ? Math.abs(num) : num;
            }
            num = Math.round(num * 100) / 100;
            parts = (num + "").split(".");
            return fixOneTwoFloat(parts[0]) + " " + getNumEnding(parts[0], intEnd) + " и " + fixOneTwoFloat(parts[1]) + " " + getNumEnding(parts[1], parts[1].length > 1 ? hundrEnd : tenthEnd);
        }
        return "неизвестно сколько";
    };

    var sayFloatUnit = function (num, endings)
    {
        return isInt(num) ? getNumEnding(num, endings) : (!isNaN(num) && typeof num == "number") ? endings[1] : endings[2];
    };

    var sayFloat = function (num, endings, prefix)
    {
        return sayFloatNum(num) + " " + (prefix ? prefix : "") + sayFloatUnit(num, endings);
    };

    var formatters =
    {
        degrees: function (num)
        {
            return sayFloat(num, degreeEnd);
        },
        milimeters: function (num)
        {
            return sayFloat(num, meterEnd, "мили");
        },
        meters: function (num)
        {
            return sayFloat(num, meterEnd);
        },
        kilometers: function (num)
        {
            return sayFloat(num, meterEnd, "кило");
        },
        knots: function (num)
        {
            return sayFloat(num, knotEnd);
        },
        inches: function (num)
        {
            return sayFloat(num, inchEnd);
        },
        feets: function (num)
        {
            return sayFloat(num, feetEnd);
        },
        miles: function (num)
        {
            return sayFloat(num, mileEnd);
        },
        minutes: function (num)
        {
            //return sayFloat(num, minuteEnd);
            return num == 0 ? "ровно" : sayFloat(num, minuteEnd);
        },
        minutes2: function (num)
        {
            //return sayFloat(num, minute2End);
            return num == 0 ? "ровно" : sayFloat(num, minuteEnd);
        },
        hours: function (num)
        {
            return sayFloat(num, hourEnd);
        },
        hectopascals: function (num)
        {
            return sayFloat(num, pascalEnd, "гекто");
        }
    };

    var convertUnits = function (num, fromUnit, toUnit, conversion)
    {
        var units = conversion.units,
            conversions = conversion.table,
            shifts = conversion.shift,
            i = units.indexOf(fromUnit.toLowerCase()),
            j = units.indexOf(toUnit.toLowerCase()),
            shift,
            result;

        if (i > -1 && j > -1)
        {
            //convert and limit to 2 decimal points
            shift = shifts ? shifts[i][j] : 0;
            result = Math.round(num * conversions[i][j] * 100 + shift) / 100;
        }
        return result;
    };

    var formatValue = function (num, fromUnit, toUnit, conversion, toInt, errorStr)
    {
        var units = conversion.units,
            i = units.indexOf(toUnit.toLowerCase()),
            ending = conversion.ending,
            result = convertUnits(num, fromUnit, toUnit, conversion);

        if (toInt)
        {
            result = Math.round(result);
        }

        if (typeof result == "number" && i > -1)
        {
            return formatters[ending[units[i]][0]](result) + ending[units[i]][1];
        }
        return errorStr;
    };

    var formatPressure = function (num, fromUnit, toUnit, toInt)
    {
        return formatValue(num, fromUnit, toUnit, pressureConversion, toInt, "давление не определено");
    };

    var formatSpeed = function (num, fromUnit, toUnit, toInt)
    {
        return formatValue(num, fromUnit, toUnit, speedConversion, toInt, "скорость не определена");
    };

    var formatTemperature = function (num, fromUnit, toUnit, toInt)
    {
        return formatValue(num, fromUnit, toUnit, temperatureConversion, toInt, "температура не определена");
    };

    var formatWindDirection = function (num)
    {
        var direction = "неизвестного направления";
        if (!isNaN(num) && typeof num == "number" && isInt(num))
        {
            if (num >= 0 && num <= 360)
            {
                num = 22.5 * Math.round(num/22.5);
                if (num == 360.0)
                {
                    direction = "северный";
                }
                else 
                {
                    for (var prop in compassDirs)
                    {
                        if (compassDirs.hasOwnProperty(prop))
                        {
                            if (compassDirs[prop] == num)
                            {
                                direction = prop;
                                break;
                            }
                        }
                    }
                }
                return direction;
            }
        }
        return direction;
    };

    return {
        format:
        {
            pressure: formatPressure,
            speed: formatSpeed,
            temperature: formatTemperature,
            wind: formatWindDirection,
            degree: formatters.degrees,
            hectopascal: formatters.hectopascals,
            milimeter: formatters.milimeters,
            meter: formatters.meters,
            kilometer: formatters.kilometers,
            knot: formatters.knots,
            inch: formatters.inches,
            feet: formatters.feets,
            mile: formatters.miles,
            minute: formatters.minutes,
            minute2: formatters.minutes2,
            hour: formatters.hours
        },
        is_num_digit: isNumDigit,
        is_alphabetic_char: isAlphabeticChar
    };
})();

App.datetime = (function ()
{
    var now = moment();

    moment.lang("ru");

    var hello = function (hour)
    {
        if (hour < 4)
        {
            return "Доброй ночи";
        } else if (hour < 12)
        {
            return "Доброе утро";
        } else if (hour < 17)
        {
            return "Добрый день";
        } else
        {
            return "Добрый вечер";
        }
    };

    var sayHello = function ()
    {
        return hello(now.get("h")) + ".";
    };

    var sayTodayDate = function ()
    {
        return now.format("[Сегодня] dddd, D MMMM.");
    };

    var sayCurrentTime = function ()
    {
        //return "Время " + moment().lang().relativeTime(now.get("h"), true, "hh") + " " + moment().lang().relativeTime(now.get("m"), true, "mm") + "."
        return "Время " + App.utils.format.hour(now.get("h")) + " " + App.utils.format.minute(now.get("m")) + "."
    };

    var sayRelativeTime = function (time)
    {
        //return moment().lang().relativeTime(time.get("h"), true, "hh") + " " + moment().lang().relativeTime(time.get("m"), true, "mm");
        return App.utils.format.hour(time.get("h")) + " " + App.utils.format.minute2(time.get("m"))
    };

    var sayAll = function ()
    {
        return sayHello() + "\n" + sayTodayDate() + "\n" + sayCurrentTime() + "\n";
    };

    return {
        say_all: sayAll,
        say_hello: sayHello,
        say_today_date: sayTodayDate,
        say_current_time: sayCurrentTime,
        say_relative_time: sayRelativeTime
    };
})();

App.metar = (function ()
{
    var output = "";

    var add_output = function (str)
    {
        output = output + str;
    };

    var decodeMetar = function (rep)
    {
        var report, date, metar;
        report = rep.split(/\r?\n/);
        if (report.length > 1)
        {
            date = report[0];
            metar = report[1];
        } else
        {
            metar = report[0];
        }
        return metar;
    };

    var decodeToken = function (token)
    {
        // Check if token is "calm wind"
        if (token == "00000KT")
        {
            add_output("Безветрие");
            add_output("\n");
            return;
        }

        // Check if token is Wind indication
        var reWindKT = /^(\d{3}|VRB)(\d{2,3})(G\d{2,3})?(KT|MPS|KMH)$/;
        if (reWindKT.test(token))
        {
            // Wind token: dddss(s){Gss(s)}KT -- ddd is true direction, ss(s) speed in knots
            var myArray = reWindKT.exec(token);
            var units = myArray[4];
            if (myArray[1] == "VRB")
            {
                add_output("Ветер переменного направления");
            } else
            {
                //add_output("Направление ветра " + App.utils.format.degree(parseInt(myArray[1], 10)));
                add_output("Ветер " + App.utils.format.wind(parseInt(myArray[1], 10)));
            }
            add_output(", скорость ветра " + App.utils.format.speed(parseInt(myArray[2], 10), units, "MPS", true));
            if (myArray[3] != null)
            {
                // I don't have the time nor the energy to investigate why
                // MSIE and Firefox behave differently with respect to an
                // omitted regular subexpression. Hence this quick hack to
                // detect if myArray[3] is not a number.
                if (myArray[3] != "")
                {
                    add_output(", с порывами " + App.utils.format.speed(parseInt(myArray[3].substr(1, myArray[3].length), 10), units, "MPS", true));
                }
            }

            add_output(".\n"); return;
        }


        // Check if token is "variable wind direction"
        var reVariableWind = /^(\d{3})V(\d{3})$/;
        if (reVariableWind.test(token))
        {
            // Variable wind direction: aaaVbbb, aaa and bbb are directions in clockwise order
            add_output("Направление ветра изменяется в пределах от " + parseInt(token.substr(0, 3)) + " до " + App.utils.format.degree(parseInt(token.substr(4, 3), 10)) + ".\n");
            return;
        }


        // Check if token is visibility
        var reVis = /^(\d{4})(N|S)?(E|W)?$/;
        if (reVis.test(token))
        {
            var myArray = reVis.exec(token);
            add_output("Видимость ");
            if (myArray[1] == "9999")
            {
                add_output("более чем " + App.utils.format.kilometer(10));
            }
            else if (myArray[1] == "0000")
            {
                add_output("менее чем " + App.utils.format.meter(50));
            }
            else
            {
                add_output(App.utils.format.meter(parseInt(myArray[1], 10)));
            }

            var dir = "";
            if (typeof myArray[2] != "undefined")
            {
                dir = dir + myArray[2];
            }
            if (typeof myArray[3] != "undefined")
            {
                dir = dir + myArray[3];
            }
            if (dir != "")
            {
                add_output(" в ");
                if (dir == "N")
                {
                    add_output("северном");
                }
                else if (dir == "NE")
                {
                    add_output("северо-восточном");
                }
                else if (dir == "E")
                {
                    add_output("восточном");
                }
                else if (dir == "SE")
                {
                    add_output("юго-восточном");
                }
                else if (dir == "S")
                {
                    add_output("южном");
                }
                else if (dir == "SW")
                {
                    add_output("юго-западном");
                }
                else if (dir == "W")
                {
                    add_output("западном");
                }
                else if (dir == "NW")
                {
                    add_output("северо-западном");
                }
                add_output(" направлении");
            }
            add_output(".\n"); return;
        }

        // Check if token is Statute-Miles visibility
        var reVisUS = /(SM)$/;
        if (reVisUS.test(token))
        {
            add_output("Видимость");
            var myVisArray = token.split("S");
            add_output(App.utils.format.mile(parseInt(myVisArray[0]), 10));
            add_output(".\n");
        }


        // Check if token is QNH indication in mmHg or hPa
        var reQNHhPa = /Q\d{3,4}/;
        if (reQNHhPa.test(token))
        {
            // QNH token: Qpppp -- pppp is pressure hPa 
            add_output("Относительное давление ");
            add_output(App.utils.format.pressure(parseInt(token.substr(1, 4), 10), "hPa", "hPa", true));
            add_output(".\n"); return;
        }

        // Check if token is QNH indication in mmHg: Annnn
        var reINHg = /A\d{4}/;
        if (reINHg.test(token))
        {
            add_output("Относительное давление: ");
            add_output(App.utils.format.pressure(parseFloat(token.substr(1, 2) + "." + token.substr(3, 4)), "inHg", "hPa", true));
            add_output(".\n"); return;
        }

        // Check if token is runway visual range (RVR) indication
        var reRVR = /^R(\d{2})(R|C|L)?\/(M|P)?(\d{4})(V\d{4})?(U|D|N)?$/;
        if (reRVR.test(token))
        {
            var myArray = reRVR.exec(token);
            add_output("Видимость на ВэПэПэ ");
            add_output(myArray[1]);
            if (typeof myArray[2] != "undefined")
            {
                if (myArray[2] == "L")
                {
                    add_output(" левая");
                } else if (myArray[2] == "R")
                {
                    add_output(" правая");
                }
                else if (myArray[2] == "C")
                {
                    add_output(" центральная");
                }
            }
            add_output(" ");
            if (typeof myArray[5] != "undefined")
            {
                // Variable range
                add_output(" изменяется от минимальной ");
                if (myArray[3] == "P")
                {
                    add_output("более чем ");
                }
                else if (myArray[3] == "M")
                {
                    add_output("менее чем ");
                }
                add_output(App.utils.format.meter(myArray[4].replace(/^0+/, '')));
                add_output(" до максимальной " + App.utils.format.meter(myArray[5].substr(1, myArray[5].length)));
            }
            else
            {
                // Single value
                if ( /*(typeof myArray[3] != "undefined") && */
              (typeof myArray[4] != "undefined"))
                {
                    if (myArray[3] == "P")
                    {
                        add_output("более чем ");
                    }
                    else if (myArray[3] == "M")
                    {
                        add_output("менее чем ");
                    }
                    add_output(App.utils.format.meter(myArray[4].replace(/^0+/, '')));
                }
            }
            if ((myArray.length > 5) && (typeof myArray[6] != "undefined"))
            {
                if (myArray[6] == "U")
                {
                    add_output(", и увеличивается");
                }
                else if (myArray[6] == "D")
                {
                    add_output(", и уменьшается");
                }
            }
            add_output(".\n");
            return;
        }


        // Check if token is CAVOK
        if (token == "CAVOK")
        {
            //add_output("Visibility 10 km or more,\n    no cloud below 5.000 feet or below the MSA (whichever is greater), \n    no cumulonimbus, and no significant weather fenomena in\n    the aerodrome or its vicinity\n");
            add_output("Горизонтальная видимость у поверхности земли 10 километров и более, нет облаков ниже 1500 метров или ниже верхнего предела минимальной высоты в секторе и отсутствуют кучево-дождевые облака, нет осадков и других природных явлений.\n");
            return;
        }


        // Check if token is NOSIG
        if (token == "NOSIG")
        {
            add_output("В ближайшие 2 часа не предвидится серьёзных изменений погоды.\n");
            return;
        }


        // Check if token is a present weather code - The regular expression is a bit
        // long, because several precipitation types can be joined in a token, and I
        // don't see a better way to get all the codes.
        var reWX = /^(\-|\+)?(VC)?(MI|BC|BL|SH|TS|FZ|PR)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS|BR|FG|FU|VA|DU|SA|HZ|PO|SQ|FC|SS|DS)$/;
        if (reWX.test(token))
        {
            //add_output("Погода: ");
            var myArray = reWX.exec(token);
            for (var i = 1; i < myArray.length; i++)
            {
                if (myArray[i] == "-") add_output("слабый ");
                if (myArray[i] == "+") add_output("сильный ");
                if (myArray[i] == "VC") add_output("в непосредственной близости, ");
                if (myArray[i] == "MI") add_output("поверхностный ");
                if (myArray[i] == "BC") add_output("частично ");
                if (myArray[i] == "SH") add_output("обильный ");
                if (myArray[i] == "TS") add_output("грозовой ");
                if (myArray[i] == "FZ") add_output("ледяной ");
                if (myArray[i] == "PR") add_output("участками ");
                if (myArray[i] == "DZ") add_output("морось ");
                if (myArray[i] == "RA") add_output("дождь ");
                if (myArray[i] == "SN") add_output("снег ");
                if (myArray[i] == "SG") add_output("изморось ");
                if (myArray[i] == "IC") add_output("ледяная пыль ");
                if (myArray[i] == "PL") add_output("ледяная крупа ");
                if (myArray[i] == "GR") add_output("град ");
                if (myArray[i] == "GS") add_output("мелкий град или снежная крупа ");
                if (myArray[i] == "BR") add_output("мгла ");
                if (myArray[i] == "FG") add_output("туман ");
                if (myArray[i] == "FU") add_output("дым ");
                if (myArray[i] == "VA") add_output("вулканический пепел ");
                if (myArray[i] == "DU") add_output("пылевая взвесь ");
                if (myArray[i] == "SA") add_output("песок ");
                if (myArray[i] == "HZ") add_output("дымка ");
                if (myArray[i] == "PO") add_output("пылевые или песчаные вихри ");
                if (myArray[i] == "SQ") add_output("шквалистый ветер ");
                if (myArray[i] == "FC") add_output("торнадо ");
                if (myArray[i] == "SS") add_output("песчаная буря ");
                if (myArray[i] == "DS") add_output("пылевая буря ");
            }
            add_output(".\n"); return;
        }


        // Check if token is recent weather observation
        var reREWX = /^RE(\-|\+)?(VC)?(MI|BC|BL|SH|TS|FZ|PR)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS)?(DZ|RA|SN|SG|IC|PL|GR|GS|BR|FG|FU|VA|DU|SA|HZ|PO|SQ|FC|SS|DS)?$/;
        if (reREWX.test(token))
        {
            add_output("После предыдущего наблюдения (но не в настоящее время), наблюдались следующие метеорологические явления: ");
            var myArray = reREWX.exec(token);
            for (var i = 1; i < myArray.length; i++)
            {
                if (myArray[i] == "-") add_output("слабый ");
                if (myArray[i] == "+") add_output("сильный ");
                if (myArray[i] == "VC") add_output("в непосредственной близости, ");
                if (myArray[i] == "MI") add_output("поверхностный ");
                if (myArray[i] == "BC") add_output("частично ");
                if (myArray[i] == "SH") add_output("обильный ");
                if (myArray[i] == "TS") add_output("грозовой ");
                if (myArray[i] == "FZ") add_output("ледяной ");
                if (myArray[i] == "PR") add_output("участками ");
                if (myArray[i] == "DZ") add_output("морось ");
                if (myArray[i] == "RA") add_output("дождь ");
                if (myArray[i] == "SN") add_output("снег ");
                if (myArray[i] == "SG") add_output("изморось ");
                if (myArray[i] == "IC") add_output("ледяная пыль ");
                if (myArray[i] == "PL") add_output("ледяная крупа ");
                if (myArray[i] == "GR") add_output("град ");
                if (myArray[i] == "GS") add_output("мелкий град или снежная крупа ");
                if (myArray[i] == "BR") add_output("мгла ");
                if (myArray[i] == "FG") add_output("туман ");
                if (myArray[i] == "FU") add_output("дым ");
                if (myArray[i] == "VA") add_output("вулканический пепел ");
                if (myArray[i] == "DU") add_output("пылевая взвесь ");
                if (myArray[i] == "SA") add_output("песок ");
                if (myArray[i] == "HZ") add_output("дымка ");
                if (myArray[i] == "PO") add_output("пылевые или песчаные вихри ");
                if (myArray[i] == "SQ") add_output("шквалистый ветер ");
                if (myArray[i] == "FC") add_output("торнадо ");
                if (myArray[i] == "SS") add_output("песчаная буря ");
                if (myArray[i] == "DS") add_output("пылевая буря ");
            }
            add_output(".\n");
            return;
        }


        // Check if token is temperature / dewpoint pair
        var reTempDew = /^(M?\d\d|\/\/)\/(M?\d\d)?$/;
        if (reTempDew.test(token))
        {
            var myArray = reTempDew.exec(token),
                sign1 = (myArray[1].charAt(0) == "M") ? -1 : 1
            sign2 = (myArray[2].charAt(0) == "M") ? -1 : 1;

            add_output("Температура " + App.utils.format.temperature(sign1 * parseInt(myArray[1].substr(sign1 > 0 ? 0 : 1, 2), 10), "C", "C", true));
            add_output(".\n");
            if (myArray[2] != "")
            {
                add_output("Точка росы " + App.utils.format.temperature(sign2 * parseInt(myArray[2].substr(sign2 > 0 ? 0 : 1, 2), 10), "C", "C", true));
            }

            add_output(".\n");
            return;
        }


        // Check if token is "sky clear" indication
        if (token == "SKC")
        {
            add_output("Чистое небо.\n");
            return;
        }


        // Check if token is "vertical visibility" indication
        var reVV = /^VV(\d{3}|\/{3})$/;
        if (reVV.test(token))
        {
            // VVddd -- ddd is vertical distance, or /// if unspecified
            var myArray = reVV.exec(token);
            add_output("Небо закрыто облаками, вертикальная видимость ");
            if (myArray[1] == "///")
            {
                add_output(" не может быть оценена.\n");
            }
            else
            {
                //add_output(": " + (100 * parseInt(myArray[1], 10)) + " feet\n");
                add_output(App.utils.format.meter(30 * parseInt(myArray[1], 10)) + ".\n");
            }
            return;
        }


        // Check if token is cloud indication
        var reCloud = /^(FEW|SCT|BKN|OVC)(\d{3})(CB|TCU)?$/;
        if (reCloud.test(token))
        {
            // Clouds: aaadddkk -- aaa indicates amount of sky covered, ddd distance over
            //                     aerodrome level, and kk the type of cloud.
            var myArray = reCloud.exec(token);
            if (myArray[1] == "FEW")
            {
                add_output("Незначительная");
            }
            else if (myArray[1] == "SCT")
            {
                add_output("Рассеянная");
            }
            else if (myArray[1] == "BKN")
            {
                add_output("Разорванная");
            }
            else if (myArray[1] == "OVC")
            {
                add_output("Сплошная");
            }
            add_output(" облачность ");

            //add_output(", at " + (100 * parseInt(myArray[2], 10)) + " feet above aerodrome level");
            //add_output("на высоте " + App.utils.format.meter(30 * parseInt(myArray[2], 10)) + " над уровнем аэродрома");
            add_output("на высоте " + App.utils.format.meter(30 * parseInt(myArray[2], 10)));

            if (myArray[3] == "CB")
            {
                add_output(", кучево-дождевые облака");
            }
            else if (myArray[3] == "TCU")
            {
                add_output(", высокие кучевые облака");
            }

            add_output(".\n"); return;
        }


        // Check if token is part of a wind-shear indication
        var reRWY = /^RWY(\d{2})(L|C|R)?$/;
        if (token == "WS")
        {
            add_output("Существует сдвиг ветра на "); return;
        }
        else if (token == "ALL")
        {
            add_output("всех "); return;
        }
        else if (token == "RWY")
        {
            add_output("ВэПэПэ.\n"); return;
        }
        else if (reRWY.test(token))
        {
            var myArray = reRWY.exec(token);
            add_output("ВэПэПэ " + myArray[1]);
            if (myArray[2] == "L") add_output(" левая");
            else if (myArray[2] == "C") add_output(" центральная");
            else if (myArray[2] == "R") add_output(" правая");
            add_output(".\n");
            return;
        }


        // Check if token is no-significant-weather indication
        if (token == "NSW")
        {
            add_output("В настоящее время не наблюдается никаких существенных явления погоды.\n");
            return;
        }


        // Check if token is no-significant-clouds indication
        if (token == "NSC")
        {
            add_output("Не наблюдается никаких существенных облаков ниже тысячи пятисот метров или ниже минимальной высоты в секторе.\n");
            return;
        }


        // Check if token is part of trend indication
        if (token == "BECMG")
        {
            add_output("В скором времени ожидаются следующие явления погоды: \n");
            return;
        }
        if (token == "TEMPO")
        {
            add_output("Ожидаются следующие временные явления погоды: \n");
            return;
        }
        var reFM = /^FM(\d{2})(\d{2})Z?$/;
        if (reFM.test(token))
        {
            var myArray = reFM.exec(token);
            add_output("начиная с " + myArray[1] + ":" + myArray[2] + " Ю Ти Си, ");
            return;
        }
        var reTL = /^TL(\d{2})(\d{2})Z?$/;
        if (reTL.test(token))
        {
            var myArray = reTL.exec(token);
            add_output("до " + myArray[1] + ":" + myArray[2] + " Ю Ти Си, ");
            return;
        }
        var reAT = /^AT(\d{2})(\d{2})Z?$/;
        if (reAT.test(token))
        {
            var myArray = reAT.exec(token);
            add_output("в " + myArray[1] + ":" + myArray[2] + " Ю Ти Си, ");
            return;
        }

        // Check if item is runway state group
        var reRSG = /^(\d\d)(\d|C|\/)(\d|L|\/)(\d\d|RD|\/)(\d\d)$/;
        if (reRSG.test(token))
        {
            var myArray = reRSG.exec(token);
            add_output("Состояние покрытия:\n");

            // Runway designator (first 2 digits)
            var r = parseInt(myArray[1], 10);
            if (r < 50) add_output("ВэПэПэ " + myArray[1] + " (или " + myArray[1] + " левая): ");
            else if (r < 88) add_output("ВэПэПэ " + (r - 50) + " Правая: ");
            else if (r == 88) add_output("Все ВэПэПэ: ");

            // Check if "CLRD" occurs in digits 3-6
            if (token.substr(2, 4) == "CLRD") add_output("чисто, ");
            else
            {
                // Runway deposits (third digit)
                if (myArray[2] == "0") add_output("чисто и сухо, ");
                else if (myArray[2] == "1") add_output("влажно, ");
                else if (myArray[2] == "2") add_output("мокро или местами вода, ");
                else if (myArray[2] == "3") add_output("иней или изморозь, ");
                else if (myArray[2] == "4") add_output("сухой снег, ");
                else if (myArray[2] == "5") add_output("мокрый снег, ");
                else if (myArray[2] == "6") add_output("слякоть, ");
                else if (myArray[2] == "7") add_output("лед, ");
                else if (myArray[2] == "8") add_output("уплотненный или укатанный снег, ");
                else if (myArray[2] == "9") add_output("замершая неровная поверхность, ");
                else if (myArray[2] == "/") add_output("нет данных, ");

                // Extent of runway contamination (fourth digit)
                if (myArray[3] == "1") add_output("степень покрытия 10% или меньше, ");
                else if (myArray[3] == "2") add_output("степень покрытия от 11% до 25%, ");
                else if (myArray[3] == "5") add_output("степень покрытия от 26% до 50%, ");
                else if (myArray[3] == "9") add_output("степень покрытия от 51% до 100%, ");
                else if (myArray[3] == "/") add_output("степень покрытия не сообщается, ");

                // Depth of deposit (fifth and sixth digits)
                if (myArray[4] == "//") add_output("толщина покрытия не сообщается, ");
                else
                {
                    var d = parseInt(myArray[4], 10);
                    if (d == 0) add_output("толщина покрытия менне чем 1 милиметр, ");
                    else if ((d > 0) && (d < 91)) add_output("толщина покрытия " + App.utils.format.milimeter(d) + ", ");
                    else if (d == 92) add_output("толщина покрытия 10 сантиметров, ");
                    else if (d == 93) add_output("толщина покрытия 15 сантиметров, ");
                    else if (d == 94) add_output("толщина покрытия 20 сантиметров, ");
                    else if (d == 95) add_output("толщина покрытия 25 сантиметров, ");
                    else if (d == 96) add_output("толщина покрытия 30 сантиметров, ");
                    else if (d == 97) add_output("толщина покрытия 35 сантиметров, ");
                    else if (d == 98) add_output("толщина покрытия 40 или более сантиметров, ");
                    else if (d == 99) add_output("одна или более ВэПэПэ не работает в связи с очисткой, ");
                }
            }

            // Friction coefficient or braking action (seventh and eighth digit)
            if (myArray[5] == "//") add_output("эффективность торможения не сообщается");
            else
            {
                var b = parseInt(myArray[5], 10);
                if (b < 91) add_output("коэффициент сцепления 0." + myArray[5]);
                else
                {
                    if (b == 91) add_output("эффективность торможения плохая");
                    else if (b == 92) add_output("эффективность торможения плохая или средняя ");
                    else if (b == 93) add_output("эффективность торможения средняя");
                    else if (b == 94) add_output("эффективность торможения средняя или хорошая");
                    else if (b == 95) add_output("эффективность торможения хорошая");
                    else if (b == 99) add_output("измерение эффективности торможения ненадежно");
                }
            }
            add_output(".\n"); return;
        }

        if (token == "SNOCLO")
        {
            add_output("Аэродром закрыт в связи со снегом на ВэПэПэ.\n");
            return;
        }

        // Check if item is sea status indication
        reSea = /^W(M)?(\d\d)\/S(\d)/;
        if (reSea.test(token))
        {
            var myArray = reSea.exec(token),
                sign = (myArray[1].charAt(0) == "M") ? -1 : 1;
            add_output("Температура поверхности моря ");
            add_output(App.utils.format.temperature(sign * parseInt(myArray[2], 10), "C", "C", true));
            add_output(".\n");
            return;
        }
    };

    var metarDecode = function (metar)
    {
        var numToken = 0,
            arrayOfTokens,
            reTime = /^\d\d:\d\d/,
            reDate = /^\d\d\d\d\/\d\d\/\d\d/,
            equalPosition = metar.indexOf("=");

        // An '=' finishes the report
        if (equalPosition > -1)
        {
            metar = metar.substr(0, equalPosition);
        }

        arrayOfTokens = metar.split(" ");

        // Check if initial token is non-METAR date
        if (reDate.test(arrayOfTokens[numToken]))
        {
            numToken++;
        }

        // Check if initial token is non-METAR time
        if (reTime.test(arrayOfTokens[numToken]))
        {
            numToken++;
        }

        // Check if initial token indicates type of report
        if (arrayOfTokens[numToken] == "METAR")
        {
            numToken++;
        }
        else if (arrayOfTokens[numToken] == "SPECI")
        {
            add_output("Прослушайте специальную метеосводку.\n");
            numToken++;
        }

        // Parse location token
        if (arrayOfTokens[numToken].length == 4)
        {
            //add_output("Расположение: " + arrayOfTokens[numToken] + "\n");
            numToken++;
        }
        else
        {
            add_output("Неверная метеосводка: неверный маркер расположения '" + arrayOfTokens[numToken] + "' \n-- должен иметь 4 символа!");
            return;
        }


        // Parse date-time token -- we allow time specifications without final 'Z'
        if ((
           ((arrayOfTokens[numToken].length == 7) &&
             (arrayOfTokens[numToken].charAt(6) == 'Z')) ||
           (arrayOfTokens[numToken].length == 6)
         ) &&
         App.utils.is_num_digit(arrayOfTokens[numToken].charAt(0)) &&
         App.utils.is_num_digit(arrayOfTokens[numToken].charAt(1)) &&
         App.utils.is_num_digit(arrayOfTokens[numToken].charAt(2)) &&
         App.utils.is_num_digit(arrayOfTokens[numToken].charAt(3)) &&
         App.utils.is_num_digit(arrayOfTokens[numToken].charAt(4)) &&
         App.utils.is_num_digit(arrayOfTokens[numToken].charAt(5)))
        {
            //add_output("День месяца: " + arrayOfTokens[numToken].substr(0, 2) + "\n");
            //add_output("Состояние погоды на " + arrayOfTokens[numToken].substr(2, 2) + ":" + arrayOfTokens[numToken].substr(4, 2) + " Ю Ти Си");
            var metarTime = moment.utc({hour: parseInt(arrayOfTokens[numToken].substr(2, 2), 10), minute: parseInt(arrayOfTokens[numToken].substr(4, 2), 10)});
            add_output("Состояние погоды на " + App.datetime.say_relative_time(metarTime.local()));

            if (arrayOfTokens[numToken].length == 6)
            {
                add_output(" (Неверное время!)");
            }
            
            add_output(".\n");
            numToken++;
        }
        else
        {
            add_output("Маркер времени не найден или с неправильным форматом!");
            return;
        }

        // Check if "AUTO" or "COR" token comes next.
        if (arrayOfTokens[numToken] == "AUTO")
        {
            add_output("Полностью автоматическая метеосводка, без вмешательства или надзора человека.\n");
            numToken++;
        }
        else if (arrayOfTokens[numToken] == "COR")
        {
            add_output("Метеосводка является коррекцией по докладу METAR или SPECI.\n");
            numToken++;
        }

        // Parse remaining tokens
        for (var i = numToken; i < arrayOfTokens.length; i++)
        {
            if (arrayOfTokens[i].length > 0)
            {
                decodeToken(arrayOfTokens[i].toUpperCase());
            }
            else
            {
                add_output("Следующий маркер имеет нулевую длину.\n");
            }
        }
    };

    var sayMetar = function (text)
    {
        metarDecode(decodeMetar(text));
        return output;
    }

    return {
        say_metar: sayMetar
    };
})();

App.run = (function ()
{
   var x = function ()
   {
   };

   return {
      y: x
   }
})();

App.process = (function ()
{
    var processMetar = function ()
    {
        //var data = "LKPR 261230Z VRB02KT 3000 BR FEW007 OVC023 M10/M11 Q1009 R24/490195 R30/490195 NOSIG";
        //var data = "LKPR 261430Z 08003KT 360V110 3600 -SN BR OVC016 M09/M11 Q1009 R24/490195 R30/490195 NOSIG"
        
        http.get(App.settings.url, function (res)
        {
            var data = "";
            res.on("data", function (chunk)
            {
                data += chunk;
            });
            res.on("end", function ()
            {
                /*
                var echo = spawn("echo", [App.datetime.say_all() + App.metar.say_metar(data)]),
                    rhvoice = spawn("RHVoice"),
                    aplay = spawn("aplay", ["-q"]);
                
                echo.stdout.on("data", function (data)
                {
                    rhvoice.stdin.write(data);
                });
                rhvoice.stdout.on("data", function (data)
                {
                    aplay.stdin.write(data);
                });
                */
                //console.log(App.datetime.say_all() + App.metar.say_metar(data));
                console.log(App.metar.say_metar(data));
            });
        }).on("error", function (e)
        {
            console.log(App.datetime.say_all() + "Произошла ошибка при получении метеосводки: " + e.message);
        });
        //console.log(App.datetime.say_all() + App.metar.say_metar(data));
    };

    return {
        start: processMetar
    }
})();


App.process.start();

/*
console.log(App.datetime.say_all());
console.log(App.utils.format.speed(-0.1, "KT", "KT"));
console.log(App.utils.format.speed(-1.2, "KT", "KT"));
console.log(App.utils.format.speed(11.12, "KT", "KT"));
console.log(App.utils.format.speed(21.12, "KT", "KT"));
console.log(App.utils.format.speed(2432.21, "KT", "KT"));
console.log(App.utils.format.speed(-2, "KT", "KT"));
console.log(App.utils.format.speed(-0, "KT", "KT"));
console.log(App.utils.format.speed(0, "KT", "KT"));
console.log(App.utils.format.speed(-23.45, "KT", "KT"));
console.log(App.utils.format.speed("ss", "KT", "KT"));
console.log(App.utils.format.speed(-26454.49999, "KT", "KT"));
*/




