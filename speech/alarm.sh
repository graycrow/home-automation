#!/bin/bash

#Чтобы поставить будильник, вводим в терминале команду "crontab -e"
#И прописываем нужные параметры:
## m h  dom mon dow   command
#0 7 * * 1-5 ./alarm.sh
#0 10 * * 6-7 ./alarm.sh


export DISPLAY=:0
export LANG=ru_RU.UTF-8


# Выбираем нужный синтезатор речи, раскомментировав одну из строк:
sayit () { espeak -vru -s130; }
#sayit () { festival --tts --language russian; }
#sayit () { festival_client --ttw | aplay -q; }
#sayit () { RHVoice | aplay -q; }


# Склоняем в соответствующем падеже слова "час" и "минута"

check_date ()

{

HOUR=`date +%H`
MIN=`date +%M`

if [[ "$HOUR" = 1[1234] || "$HOUR" = ?[056789] ]]; then LC_HOUR="часов"
elif [[ "$HOUR" = ?[234] ]]; then LC_HOUR="час+а"
elif [[ "$HOUR" = ?1 ]]; then LC_HOUR="час"
else LC_HOUR="значение не определено"
fi

if [[ "$MIN" = 1[1234] || "$MIN" = ?[056789] ]]; then LC_MINUTE="минут"
elif [[ "$MIN" = ?[34] ]]; then LC_MINUTE="минуты"
else LC_MINUTE="значение не определено"
fi

if [[ "$MIN" = 01 ]]; then TIME="$HOUR $LC_HOUR однa минута"
elif [[ "$MIN" = 11 ]]; then TIME="$HOUR $LC_HOUR $MIN $LC_MINUTE"
elif [[ "$MIN" = ?1 ]]; then TIME="$HOUR $LC_HOUR $(($MIN-1)) одна минута"
elif [[ "$MIN" = 02 ]]; then TIME="$HOUR $LC_HOUR две минуты"
elif [[ "$MIN" = 12 ]]; then TIME="$HOUR $LC_HOUR $MIN $LC_MINUTE"
elif [[ "$MIN" = ?2 ]]; then TIME="$HOUR $LC_HOUR $(($MIN-2)) две минуты"
elif [[ "$MIN" = 00 ]]; then TIME="$HOUR $LC_HOUR ровно"
else TIME="$HOUR $LC_HOUR $MIN $LC_MINUTE"
fi

if [[ "$HOUR" = 08 ]] || [[ "$HOUR" -ge 06 && "$HOUR" -le 11 ]]; then
HELLO="Доброе утро"
elif [[ "$HOUR" -ge 12 && "$HOUR" -le 17 ]]; then
HELLO="Добрый день"
elif [[ "$HOUR" -ge 18 && "$HOUR" -le 23 ]]; then
HELLO="Добрый вечер"
elif [[ "$HOUR" -ge 00 && "$HOUR" -le 05 ]]; then
HELLO="Доброй ночи"
else HELLO="Привет"
fi

}


# Выясняем, в каком городе мы находимся

#CITY=`wget -q -O - 2ip.ru | grep -P geoip | sed 's/<[^>]*>//g' | awk '{print $3}'`

#CITY_ID=`wget -q -O - http://bar.gismeteo.ru/gmbartlist.xml | iconv -f cp1251 -t utf8 | grep $CITY | awk '{print $3}' | sed 's/[i="]//g'`

#TMP_FILE="/tmp/gismeteo_$CITY_ID"

#URL="http://www.gismeteo.ru/ztowns/$CITY_ID.htm" #CITY_ID - не тот, к которому привыкли, но перенаправляет, куда надо. Информер: http://informer.gismeteo.ru/xml/$CITY_ID_1.xml


# Получаем температуру в своем городе

#wget -q -O - $URL > $TMP_FILE

#TEMP="`grep -m 1 'value m_temp c' $TMP_FILE | sed 's/<[^>]*>/ /g' | awk '{print $1}'`"

if [[ `echo $TEMP | grep minus` ]]; then TEMP_SIGN="минус"
else TEMP_SIGN=""
fi

DEGREE="`echo $TEMP | grep -Po '[0-9]{1,2}'`"


# Проверяем погодные условия

#WEATH="`grep -m 1 '<dd>' $TMP_FILE | sed 's/[ \t]*//;s/<[^>]*>//g'`"


# Склоняем в соответствующем падеже слово "градус"

if [[ "$DEGREE" = 1[1234] || "$DEGREE" = *[056789] ]]; then LC_DEGREE="градусов"
elif [[ "$DEGREE" = *[234] ]]; then LC_DEGREE="градуса"
elif [[ "$DEGREE" = *1 ]]; then LC_DEGREE="градус"
else LC_DEGREE="значение не определено"
fi


# А теперь сам будильник

#playlist="`qdbus --literal org.mpris.MediaPlayer2.rhythmbox /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Playlists.GetPlaylists 1 1 0 0 | sed -e :a -e 's/\][^>]*\]//g;s/\[[^>]*\[ObjectPath: //g'`" #Ищем динамический плейлист "Любимые композиции"
#qdbus org.mpris.MediaPlayer2.rhythmbox /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Playlists.ActivatePlaylist $playlist #Запускаем найденный плейлист
#rhythmbox-client --next

#rhythmbox-client --play-uri="http://online.radiorecord.ru:8102/tm_128" #Можно и радио послушать

check_date
echo "$HELLO, Хозяин. Сегодня `date +%A`. Время $TIME. Температура за окном $TEMP_SIGN $DEGREE $LC_DEGREE, $WEATH" | sayit
exit 0