var express = require("express"),
    routes = require("./routes"),
    http = require("http"),
    path = require("path"),
    ejs = require("ejs"),
    favicon = require("serve-favicon"),
    logger = require("morgan"),
    cookieParser = require("cookie-parser"),
    bodyParser = require("body-parser");

var routes = require("./routes/index");
var app = express();

app.set("port", process.env.PORT || 8080);
app.set("views", path.join(__dirname, "views"));
//app.set("view engine", "ejs");
app.set("view engine", "html");
app.engine("html", ejs.renderFile);
app.use(favicon(path.join(__dirname, "public", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use("/", routes);

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render("error", {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
        message: err.message,
        error: {}
    });
});

module.exports = app;
	