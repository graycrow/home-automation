/*jslint browser: true, node: true, nomen: true, white: true */
Graycrow.Razberry.SectionsProvider = function ()
{
    "use strict";
    //config
    var sections = [];

    this.setSections = function (sectionsDefinition)
    {
        var section;
        for (section in sectionsDefinition)
        {
             this.setSection(sectionsDefinition[section]);
        }
    };

    this.setSection = function (section)
    {
        sections.push(section);
    };

    //injected
    this.$get = function ()
    {
        return sections;
    };
};