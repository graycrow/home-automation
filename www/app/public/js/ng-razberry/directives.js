/*jslint browser: true, node: true, nomen: true, white: true */
Graycrow.Razberry.AccordionDirective = function ()
{
    return {
        restrict: "EA",
        controller: "AccordionCtrl",
        transclude: true,
        replace: true,
        templateUrl: "accordion-template.html"
    };
};

Graycrow.Razberry.AccordionSectionDirective = ["$parse", function ($parse)
{
    return {
        require: "^accordion",
        restrict: "EA",
        transclude: true,
        replace: true,
        templateUrl: "accordion-section-template.html",
        scope: { section: "=" },
        link: function (scope, element, attrs, accordionCtrl)
        {
            accordionCtrl.addSection(scope);
            scope.isOpen = undefined;
            scope.$watch("isOpen", function (value)
            {
                if (value == undefined)
                {
                    element.removeClass("col-sm-1 col-lg-1 col-sm-9 col-lg-9").addClass("col-sm-3 col-lg-3");
                    accordionCtrl.reset();
                }
                else if (value)
                {
                    accordionCtrl.closeOthers(scope);
                    element.removeClass("col-sm-1 col-lg-1 col-sm-3 col-lg-3").addClass("col-sm-9 col-lg-9");
                }
                else
                {
                    element.removeClass("col-sm-3 col-lg-3 col-sm-9 col-lg-9").addClass("col-sm-1 col-lg-1");
                }
            });
        }
    };
} ];
