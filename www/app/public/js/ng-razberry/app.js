/*jslint browser: true, node: true, nomen: true, white: true */

var Graycrow = Graycrow || {};
Graycrow.Razberry = {};

Graycrow.Razberry.SectionsConfig = [
    {
        name: "Sensors",
        icon: "dashboard",
        cssClass: "one"
    },
    {
        name: "Zones",
        icon: "lightbulb-o",
        cssClass: "two"
    },
    {
        name: "Automation",
        icon: "tasks",
        cssClass: "three"
    },
    {
        name: "Settings",
        icon: "cog",
        cssClass: "four"
    },
];