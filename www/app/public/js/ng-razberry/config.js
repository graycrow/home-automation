/*globals angular */
/*jslint browser: true, node: true, nomen: true, white: true */
Graycrow.RazberryApp = (function ()
{
    "use strict";
    var init, start;

    init = function (appName, payload)
    {
        //create the module
        var RazberryApp = angular.module(appName, ["ngRoute", "ngResource"]);

        //configure routes
        RazberryApp.config(["$routeProvider", function ($routeProvider)
        {
            $routeProvider
                .when("/", {
                    templateUrl: "home-template.html",
                    controller: "HomeCtrl"
                })
                .otherwise({
                    template: "<h1>Not Found</h1>"
                });
        } ]);

        //set the provider
        RazberryApp.provider("Sections", Graycrow.Razberry.SectionsProvider);

        //configure the provider
        RazberryApp.config(["SectionsProvider", function (SectionsProvider)
        {
            if (!payload)
            {
                throw "Need to have sections";
            }
            SectionsProvider.setSections(payload);
        } ]);


        //wireup the directives
        RazberryApp.directive("accordion", Graycrow.Razberry.AccordionDirective);
        RazberryApp.directive("accordionSection", Graycrow.Razberry.AccordionSectionDirective);

        //wireup the controllers
        RazberryApp.controller("HomeCtrl", Graycrow.Razberry.HomeController);
        RazberryApp.controller("AccordionCtrl", Graycrow.Razberry.AccordionController);
        return RazberryApp;
    };

    //start 'em up
    start = function (appName, payload)
    {
        //initialize the app
        var module = init(appName, payload);

        //startup Angular
        angular.bootstrap(document, [appName]);
        return module;
    };

    return {
        start: start
    };
})();
