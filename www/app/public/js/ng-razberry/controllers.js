/*jslint browser: true, node: true, nomen: true, white: true */

Graycrow.Razberry.HomeController = ["$scope", "Sections", function ($scope, Sections)
{
    "use strict";
    $scope.sections = Sections;
} ];

Graycrow.Razberry.AccordionController = ["$scope", function ($scope)
{
    this.sections = [];

    this.reset = function ()
    {
        angular.forEach(this.sections, function (section)
        {
            section.isOpen = undefined;
        });
    };

    this.closeOthers = function (openSection)
    {
        angular.forEach(this.sections, function (section)
        {
            if (section !== openSection)
            {
                section.isOpen = false;
            }
        });
    };

    this.addSection = function (sectionScope)
    {
        var that = this;
        this.sections.push(sectionScope);

        sectionScope.$on('$destroy', function (event)
        {
            that.removeSection(sectionScope);
        });
    };
} ];