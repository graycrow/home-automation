﻿import { Component, View } from "angular2/angular2";

@Component({
    selector: "razberry-app"
})
@View({
    template: `<h1>Hello {{ name }}</h1>`
})
export class App {
    name: string;

    constructor() {
        this.name = "Alice";
    }
}
