﻿/// <reference path="typings/angular2/angular2.d.ts" />

import { bootstrap, RootRouter, Pipeline, bind, Router } from "angular2/angular2";
import { App } from "./app/app";

bootstrap(App);